﻿namespace DigiDoc_backend.Extensions
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class StringHelper
    {
        public static string Base64Decode(this string data)
        {
            byte[] binary = Convert.FromBase64String(data);
            return Encoding.Default.GetString(binary);
        }

        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string GetHashSha256(this string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static string ComputeHash(string plainText, string salt)
        {
            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);

            SHA256Managed hash = new SHA256Managed();

            // Compute hash value of salt.
            byte[] plainHash = hash.ComputeHash(plainTextBytes);

            byte[] concat = new byte[plainHash.Length + saltBytes.Length];

            System.Buffer.BlockCopy(saltBytes, 0, concat, 0, saltBytes.Length);
            System.Buffer.BlockCopy(plainHash, 0, concat, saltBytes.Length, plainHash.Length);

            byte[] tHashBytes = hash.ComputeHash(concat);

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(tHashBytes);

            // Return the result.
            return hashValue;
        }
    }
}