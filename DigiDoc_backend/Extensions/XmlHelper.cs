﻿namespace DigiDoc_backend.Extensions
{
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    public static class XmlHelper
    {
        public static string ConvertToCleanXmlString<T>(this T value)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(value.GetType());
            var settings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true };

            using (var stream = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, value, emptyNamepsaces);
                return stream.ToString();
            }
        }

        public static string FormatResponse(this string sourceXml)
        {
            return sourceXml.Replace("<string>", string.Empty).Replace("</string>", string.Empty).Replace("&lt;", "<").Replace("&gt;", ">");
        }

    }
}