﻿namespace DigiDoc_backend.Services
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    using DigiDoc_backend.Models;

    public class ShowDocInfoService
    {
        public List<DataFileModel> GetFiles(XElement element)
        {
            List<DataFileModel> prepared = new List<DataFileModel>();
            var xFiles = element.Elements("DataFileInfo");
            foreach (var xFile in xFiles)
            {
                var id = xFile.Element("Id");
                var name = xFile.Element("Filename");
                var length = xFile.Element("ContentType");
                var mime = xFile.Element("MimeType");

                prepared.Add(new DataFileModel
                                 {
                                     Id = id != null ? id.Value : string.Empty,
                                     Name = name != null ? name.Value : string.Empty,
                                     Length = length != null ? length.Value : string.Empty,
                                     Mime = mime != null ? mime.Value : string.Empty
                                 });
            }
            return prepared;
        }
    }
}