﻿namespace DigiDoc_backend.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Xml;
    using System.Xml.Linq;

    using DigiDoc_backend.Extensions;

    public static class DigiDocService
    {
        private const string RESPONSE_STATUS_OK = "OK";

        private const string URL = "https://digidocservice.sk.ee/DigiDocService";

        private const string ACTION = "";

        public static Dictionary<string, string> GetMidStatusResponseErrorMessages = new Dictionary<string, string>{
                {"EXPIRED_TRANSACTION","There was a timeout before the user could sign with Mobile ID"},
                {"USER_CANCEL","User has cancelled the signing operation."},
                {"NOT_VALID","Signature is not valid."},
                {"MID_NOT_READY","Mobile ID is not yet available for this phone. Please try again later."},
                {"PHONE_ABSENT","Mobile phone is not reachable."},
                {"SENDING_ERROR","The Mobile ID message could not be sent to the mobile phone."},
                {"SIM_ERROR","There was a problem with the mobile phones SIM card."},
                {"OCSP_UNAUTHORIZED","Mobile ID user is not authorized to make OCSP requests."},
                {"INTERNAL_ERROR","There was an internal error during signing with Mobile ID."},
                {"REVOKED_CERTIFICATE","The signers certificate is revoked."}
        };

        public static XElement StartSession(Dictionary<string, string> parameters)
        {
            return Invoke("StartSession", parameters);
        }

        public static XElement GetSignedDoc(Dictionary<string, string> parameters)
        {
            return Invoke("GetSignedDoc", parameters);
        }

        public static XElement GetSignedDocInfo(Dictionary<string, string> parameters)
        {
            return Invoke("GetSignedDocInfo", parameters);
        }

        public static XElement PrepareSignature(Dictionary<string, string> parameters)
        {
            return Invoke("PrepareSignature", parameters);
        }

        public static XElement FinalizeSignature(Dictionary<string, string> parameters)
        {
            return Invoke("FinalizeSignature", parameters);
        }

        public static XElement RemoveSignature(Dictionary<string, string> parameters)
        {
            return Invoke("RemoveSignature", parameters);
        }

        public static XElement MobileSign(Dictionary<string, string> parameters)
        {
            return Invoke("MobileSign", parameters);
        }

        public static XElement GetStatusInfo(Dictionary<string, string> parameters)
        {
            return Invoke("GetStatusInfo", parameters);
        }

        public static XElement AddDataFile(Dictionary<string, string> parameters)
        {
            return Invoke("AddDataFile", parameters);
        }

        public static XElement RemoveDataFile(Dictionary<string, string> parameters)
        {
            return Invoke("RemoveDataFile", parameters);
        }

        public static XElement CreateSignedDoc(Dictionary<string, string> parameters)
        {
            return Invoke("CreateSignedDoc", parameters);
        }

        #region Privates

        private static XElement Invoke(string serviceName, Dictionary<string, string> parameters)
        {
            var xmlRequest = GetXmlRequest(parameters);
            var soapEnvelopeXml = CreateSoapEnvelope(serviceName, xmlRequest);

            HttpWebRequest webRequest = CreateWebRequest(URL, ACTION);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                string soapResult;
                using (var rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }

                var response = soapResult.ConvertToCleanXmlString().FormatResponse();

                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
//                XNamespace dir = "http://www.sk.ee/DigiDocService/DigiDocService_2_3.wsdl";
                var xresp = XElement.Parse(response);
                var bodyResponse = xresp.Elements(soapenv + "Body").First().Elements().First();
                return bodyResponse;
            }
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static string GetXmlRequest(Dictionary<string, string> parameters)
        {
            string request = string.Empty;
            foreach (var pair in parameters)
            {
                request += string.Format("<{0}>{1}</{0}>", pair.Key, pair.Value);
            }
            return request;
        }

        private static XmlDocument CreateSoapEnvelope(string serviceName, string xmlRequest)
        {
            var soapEnvelop = new XmlDocument();

            soapEnvelop.LoadXml(string.Format(@"<soapenv:Envelope
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
xmlns:dig=""http://www.sk.ee/DigiDocService/DigiDocService_2_3.wsdl"">
    <soapenv:Header/>
    <soapenv:Body>
        <dig:{0} soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
            {1}
        </dig:{0}>
    </soapenv:Body>
</soapenv:Envelope>", serviceName, xmlRequest));
            return soapEnvelop;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        #endregion

    }
}