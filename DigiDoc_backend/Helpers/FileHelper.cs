﻿namespace DigiDoc_backend.Helpers
{
    using System.IO;

    public static class FileHelper
    {
        public static void DeleteIfExists(string path)
        {
            if (File.Exists(path)) 
                File.Delete(path);
        }
    }
}