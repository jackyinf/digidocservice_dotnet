﻿namespace DigiDoc_backend.Helpers
{
    namespace ETIS.Core.Helpers
    {
        using System.Text.RegularExpressions;

        /// <summary>
        /// Abiklass HEX stringi tüübi tuvastamiseks.
        /// </summary>
        public static class CertificateHelper
        {
            private const string SHA1 = "SHA-1";
            private const string SHA224 = "SHA-224";
            private const string SHA256 = "SHA-256";
            private const string SHA384 = "SHA-384";
            private const string SHA512 = "SHA-512";

            private const int SHA1Length = 40;
            private const int SHA224Length = 56;
            private const int SHA256Length = 64;
            private const int SHA384Length = 96;
            private const int SHA512Length = 128;

            /// <summary>
            /// Vastavalt HEX stringi suurusele, tagasta õige HEX tüüp.
            /// </summary>
            /// <param name="hashValue">HEX string</param>
            /// <returns>HEX stringi tüüp</returns>
            public static string GetHashType(this string hashValue)
            {
                if (string.IsNullOrWhiteSpace(hashValue) || !hashValue.OnlyHexInString())
                    return null;

                switch (hashValue.Length)
                {
                    case SHA1Length:
                        return SHA1;
                    case SHA224Length:
                        return SHA224;
                    case SHA256Length:
                        return SHA256;
                    case SHA384Length:
                        return SHA384;
                    case SHA512Length:
                        return SHA512;
                }

                return null;
            }

            public static bool OnlyHexInString(this string test)
            {
                // For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
                return Regex.IsMatch(test, @"\A\b[0-9a-fA-F]+\b\Z");
            }
        }
    }

}