﻿namespace DigiDoc_backend.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;

    using DigiDoc_backend.DigiDoc;
    using DigiDoc_backend.Services;

    using Ionic.Zip;

    using WebGrease.Css.Ast;

    public static class DocHelper
    {
        public static HttpSessionStateBase Session { get; set; }

        public static string GetNecodedHashcodeVersionOfContainer(string containerUploadInputName)
        {
            throw new NotImplementedException();
        }

        public static DigidocSession GetHashcodeSession()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method for converting container in hashcode form to a container with files.
        /// </summary>
        /// <param name="base64Content">Contents of the container. If container type is BDOC then container_data should be Base64 encoded</param>
        /// <param name="dataFiles">Array of FileSystemDataFile instances</param>
        /// <param name="pathToUploads"></param>
        /// <param name="sesscode"></param>
        /// <returns>Success</returns>
        public static bool CreateContainerWithFiles(string base64Content, List<string> dataFiles, string pathToUploads, string sesscode)
        {
            var bdocPath = Path.Combine(pathToUploads, string.Format("{0}{1}", sesscode, ".bdoc"));
            byte[] data = Convert.FromBase64String(base64Content);
            File.WriteAllBytes(bdocPath, data);

            var files = Directory.EnumerateFiles(pathToUploads).Where(dataFiles.Contains);
            using (ZipFile zipDest = ZipFile.Read(bdocPath))
            {
                //zipDest.AddFiles(System.IO.Directory.EnumerateFiles(outputDirectory)); //This will add them as a directory
                zipDest.AddFiles(files, false, string.Empty); //This will add the files to the root
                zipDest.Save();
            }
            return true;
        }

//        public static void AddDatafileViaDds(string originalFileName, string datafileMimeType, string fileContents, int size, string ddsSessionCode)
        public static void AddDatafileViaDds(HttpPostedFileBase file, string path, string sesscode)
        {
            var originalFileName = file.FileName;
            var fileContents = File.ReadAllText(path);
            var size = file.ContentLength;
            var datafileMimeType = file.ContentType;

            const string DigestType = BdocContainer.DEFAULT_HASH_ALGORITHM;
            var digestValue = BdocContainer.DatafileHashcode(fileContents);
            DigiDocService.AddDataFile(new Dictionary<string, string>
                                           {
                                               { "Sesscode", sesscode },
                                               { "FileName", originalFileName },
                                               { "MimeType", datafileMimeType },
                                               { "ContentType", "HASHCODE" },
                                               { "Size", size.ToString() },
                                               { "DigestType", DigestType },
                                               { "DigestValue", digestValue }
//                                               { "DigestValue", "OJIFkE1se7g/xnZROREibyviW/FGVha7mylYcQCrFBQ=" }
                                           });

        }

        public static string GetContainerType(string filename)
        {
            return "BDOC";
        }

        public static string GetNextDatafileId()
        {
            throw new NotImplementedException();
        }

        public static List<FileSystemDataFile> GetDatafilesFromContainer()
        {
            throw new NotImplementedException();
        }

        private static FunctionNode GetFirstMissingDatafileNoFromDds()
        {
            throw new NotImplementedException();
        }

        public static List<IDataFile> RemoveDatafile(string toBeRemovedName)
        {
            throw new NotImplementedException();
        }

        public static List<object> GetDesiredContainerType(string containerTypeInputName)
        {
            throw new NotImplementedException();
        }

        public static void PersistHashcodeSession()
        {
            throw new NotImplementedException();
        }

        public static string GetNewContainerName(string uploadedFileName, string containerType)
        {
            if (string.IsNullOrWhiteSpace(uploadedFileName) || string.IsNullOrWhiteSpace(containerType))
                return string.Empty;

            return string.Format("{0}.{1}", Path.GetFileNameWithoutExtension(uploadedFileName).ToLower(), containerType.ToLower());
        }

        public static void SetHashcodeSession()
        {
            throw new NotImplementedException();
        }

    }
}