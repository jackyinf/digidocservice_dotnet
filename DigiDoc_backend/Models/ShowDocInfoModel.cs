﻿namespace DigiDoc_backend.Models
{
    using System.Collections.Generic;

    public class ShowDocInfoModel
    {
        public string Format { get; set; }
        public string Version { get; set; }

        public List<DataFileModel> Files = new List<DataFileModel>();

        public List<SignerModel> Signers = new List<SignerModel>(); 
    }
}