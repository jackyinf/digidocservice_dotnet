namespace DigiDoc_backend.Models
{
    public class DataFileModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Mime { get; set; }
        public string Length { get; set; }
    }
}