﻿namespace DigiDoc_backend.Models
{
    public class SignerModel
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string AdditionalStatusInfo { get; set; }
        public string Time { get; set; }
        public string Role { get; set; }
        public string Signer { get; set; }
    }
}