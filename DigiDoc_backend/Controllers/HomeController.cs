﻿namespace DigiDoc_backend.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;

    using DigiDoc_backend.Helpers;
    using DigiDoc_backend.Helpers.ETIS.Core.Helpers;
    using DigiDoc_backend.Models;
    using DigiDoc_backend.Services;

    [RequireHttps]
    public class HomeController : Controller
    {
        public HomeController()
        {
            DocHelper.Session = Session;
        }

        public ActionResult Index()
        {
            var model = new IndexModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file, IndexModel model)
        {
            // Start Session
            var response = DigiDocService.StartSession(new Dictionary<string, string> { { "bHoldSession", "true" } });
            var status = response.Elements("Status").First().Value;
            if (status != "OK")
            {
                ModelState.AddModelError("START_SESSION_ERROR", new Exception("Could not start session"));
                return View();
            }
            var sesscode = response.Elements("Sesscode").First().Value;
            Session["ddsSessionCode"] = sesscode;

            // Store a data file to a more permanent place
            if (file == null || file.ContentLength <= 0)
            {
                ModelState.AddModelError("FILE IS MISSING", new Exception("File is missing"));
                return View();
            }

            // Save file into Uploads directory
            var extension = Path.GetExtension(file.FileName);
            var pathToUploads = Server.MapPath("~/Uploads/");
            var path = Path.Combine(pathToUploads, string.Format("{0}{1}", sesscode, extension));
            file.SaveAs(path);

            // Create document
            var format = model.Format.ToLower();
            const string Version = "2.1"; // hardcoded
            DigiDocService.CreateSignedDoc(new Dictionary<string, string>
                                               {
                                                   { "Sesscode", sesscode },
                                                   { "Format", format.ToUpper() }, 
                                                   { "Version", Version }    
                                               });

            // Add file into DDS session.
            DocHelper.AddDatafileViaDds(file, path, sesscode);

            // Get the HASHCODE container from DDS.
            response = DigiDocService.GetSignedDoc(new Dictionary<string, string> { { "Sesscode", sesscode } });
            var base64Content = response.Elements("SignedDocData").First().Value;

            // Save base64Content onto hard drive.
            var success = DocHelper.CreateContainerWithFiles(base64Content, new List<string> { path }, pathToUploads, sesscode);
            FileHelper.DeleteIfExists(path);

            if (success)
            {
                var action = RedirectToAction("ShowDocInfo");
                action.RouteValues.Add("sessionCode", sesscode);
                return action;
            }

            return View(model);
        }

        public ActionResult ShowDocInfo(string sessionCode)
        {
            var response = DigiDocService.GetSignedDocInfo(new Dictionary<string, string> { { "Sesscode", sessionCode } });
            var documentFileInfo = response.Element("SignedDocInfo");

            ShowDocInfoModel model = new ShowDocInfoModel();
            ShowDocInfoService service = new ShowDocInfoService();
            model.Format = documentFileInfo.Element("Format").Value;
            model.Version = documentFileInfo.Element("Version").Value;
            model.Files = service.GetFiles(documentFileInfo);

            return View(model);
        }

        [HttpPost]
        public JsonResult PrepareSignature(string signersCertificateHEX)
        {
            var sesscode = Session["ddsSessionCode"] as string ?? string.Empty;

            var response = DigiDocService.PrepareSignature(new Dictionary<string, string>
                                                {
                                                    { "Sesscode", sesscode },
                                                    { "SignersCertificate", signersCertificateHEX },
                                                    { "SignersTokenId", "" },    //?
                                                    { "SigningProfile", "" }    //?
                                                });

            var xSignatureInfoDigest = response.Element("SignedInfoDigest");
            var xSignatureId = response.Element("SignatureId");

            var signatureInfoDigest = xSignatureInfoDigest != null ? xSignatureInfoDigest.Value : string.Empty;
            var signatureId = xSignatureId != null ? xSignatureId.Value : string.Empty;
            var signatureHashType = xSignatureInfoDigest != null ? xSignatureInfoDigest.Value.GetHashType() : string.Empty;

            var jsonResponse = new
            {
                is_success = true,
                signature_info_digest = signatureInfoDigest,
                signature_id = signatureId,
                signature_hash_type = signatureHashType
            };

            return Json(jsonResponse);
        }

        [HttpPost]
        public JsonResult FinalizeSignature(string requestAct, string signatureId, string signatureValue)
        {
            var sesscode = Session["ddsSessionCode"] as string ?? string.Empty;

            var response = DigiDocService.FinalizeSignature(new Dictionary<string, string>
                                                                {
                                                                    { "Sesscode", sesscode },
                                                                    { "SignatureId", signatureId },
                                                                    { "SignatureValue" , signatureValue }
                                                                });

            // Get the HASHCODE container from DDS.
            response = DigiDocService.GetSignedDoc(new Dictionary<string, string> { { "Sesscode", sesscode } });
            var base64Content = response.Elements("SignedDocData").First().Value;

            //// Save base64Content onto hard drive.
            //var success = DocHelper.CreateContainerWithFiles(base64Content, new List<string> { path }, pathToUploads, sesscode);
            //FileHelper.DeleteIfExists(path);

            return Json(new { });
        }
    }
}