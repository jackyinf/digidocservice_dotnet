﻿namespace DigiDoc_backend.DigiDoc
{
    using System;

    public class BdocDataFile : IDataFile
    {
        private string bdocFilename;

        private string filename;

        public BdocDataFile(string bdocFilename, string filename)
        {
            bdocFilename = bdocFilename;
            filename = filename;
        }

        public string GetName()
        {
            return filename;
        }

        public int GetSize()
        {
            throw new NotImplementedException();                        
        }

        public string GetContent()
        {
            throw new NotImplementedException();                        
        }
    }
}