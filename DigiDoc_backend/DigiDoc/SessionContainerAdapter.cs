﻿namespace DigiDoc_backend.DigiDoc
{
    using System;
    using System.Collections.Generic;

    public class SessionContainerAdapter : IStringContainer
    {
        private DigidocSession session;

        private readonly IFileContainer fileContainer;

        public SessionContainerAdapter(DigidocSession session, IFileContainer fileContainer)
        {
            this.session = session;
            this.fileContainer = fileContainer;
        }

        public List<IDataFile> GetDataFiles()
        {
            return fileContainer.GetDataFiles();
        }

        public override string ToString()
        {
            return fileContainer.ToString();
        }

        public bool IsHashcodesFormat()
        {
            return fileContainer.IsHashcodesFormat();
        }

        public IStringContainer ToHashcodeFormat()
        {
            throw new NotImplementedException();
        }

        public IStringContainer ToDataFilesFormat(List<IDataFile> dataFiles)
        {
            throw new NotImplementedException();
        }
        
    }
}