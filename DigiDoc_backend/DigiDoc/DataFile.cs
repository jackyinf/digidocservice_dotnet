﻿namespace DigiDoc_backend.DigiDoc
{
    public interface IDataFile
    {
        string GetName();

        int GetSize();

        string GetContent();
    }
}