﻿namespace DigiDoc_backend.DigiDoc
{
    using System;

    public class DigidocSession
    {
        private string sessionId;

        private object configuration;

        readonly Random random = new Random();


        public DigidocSession(object configuration)
        {
            this.configuration = configuration;
        }

        public SessionContainerAdapter ContainerFromString(string data)
        {
            throw new NotImplementedException();
        }

        public string CreateFile()
        {
            throw new NotImplementedException();
            
        }

        private string RequireSessionPath()
        {
            throw new NotImplementedException();            
        }

        private string GetSessionPath()
        {
            throw new NotImplementedException();            
        }

        public string GetSessionId()
        {
            if (string.IsNullOrWhiteSpace(sessionId))
            {
                return InitSession();
            }
            return sessionId;
        }

        private string InitSession()
        {
            sessionId = random.Next(999999999).ToString(); //9-digit
            return sessionId;
        }

        /// <summary>
        /// Ends session.
        /// Ends session and deletes all local temporary files related to this session.
        /// </summary>
        public void EndSession()
        {
            DeleteSessionFiles();
        }

        public void DeleteSessionFiles()
        {
            
        }
    }
}