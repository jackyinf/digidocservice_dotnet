﻿namespace DigiDoc_backend.DigiDoc
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;

    public class HashcodesXml
    {
        const string HASH_CODES_ELEMENT_NAME = "hashcodes";
        const string FILE_ENTRY_ELEMENT_NAME = "file-entry";
        const string FILE_ENTRY_ELEMENT_ATTRIBUTE_FULL_PATH = "full-path";
        const string FILE_ENTRY_ELEMENT_ATTRIBUTE_HASH = "hash";
        const string FILE_ENTRY_ELEMENT_ATTRIBUTE_SIZE = "size";

        public static object Parse(string xml)
        {
            throw new NotImplementedException();
        }

        private static HashcodesFileEntry XmlElementToFileEntry(XmlElement fileEntry)
        {
            throw new NotImplementedException();
        }

        public static object DataFilesToHashcodesXml(List<IDataFile> dataFiles, string hashAlgorithm)
        {
            throw new NotImplementedException();            
        }

        public static HashcodesFileEntry ConvertDataFileToFileEntry(IDataFile dataFile, string hashAlgorithm)
        {
            throw new NotImplementedException();
//            return new HashcodesFileEntry(dataFile.GetName(), );
        }

        public static string GetXml(XElement element)
        {
            throw new NotImplementedException();
        }

        private static void FileEntryToXmlElement(HashcodesFileEntry fileEntry, XElement element)
        {
            throw new NotImplementedException();            
        }

    }
}