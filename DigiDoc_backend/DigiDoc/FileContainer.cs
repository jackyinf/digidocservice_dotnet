﻿namespace DigiDoc_backend.DigiDoc
{
    using System.Collections.Generic;

    public interface IFileContainer : IContainer
    {
        /// <summary>
        /// Writes container in hashcodes format to new file.
        /// </summary>
        /// <param name="hashcodesFilename">file name to write to.</param>
        /// <returns>New container, pointing to new file.</returns>
        IFileContainer WriteAsHashcodes(string hashcodesFilename);

        /// <summary>
        /// Writes container in datafiles format.
        /// </summary>
        /// <param name="filename">file name to write to.</param>
        /// <param name="dataFiles">array of {@link DataFile}-s, to replace hashcodes with.</param>
        /// <returns>New container new cointainer, pointing to new file.</returns>
        IFileContainer WriteWithDataFiles(string filename, List<IDataFile> dataFiles);
    }
}