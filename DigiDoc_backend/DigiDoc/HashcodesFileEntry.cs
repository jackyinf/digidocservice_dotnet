﻿namespace DigiDoc_backend.DigiDoc
{
    /// <summary>
    /// Class for representing file-entry tag in hashcodes-*.xml file.
    /// </summary>
    public class HashcodesFileEntry
    {
        private readonly string fullPath;

        private readonly string hash;

        private readonly int size;
       
        public HashcodesFileEntry(string fullPath, string hash, int size)
        {
            this.fullPath = fullPath;
            this.hash = hash;
            this.size = size;
        }

        public string GetFullPath()
        {
            return fullPath;
        }

        public string GetHash()
        {
            return hash;
        }

        public int GetSize()
        {
            return size;
        }
    }
}