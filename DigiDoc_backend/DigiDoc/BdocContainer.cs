﻿namespace DigiDoc_backend.DigiDoc
{
    using System;
    using System.Collections.Generic;
    using DigiDoc_backend.Extensions;

    public class BdocContainer : IFileContainer
    {
        public const string DEFAULT_HASH_ALGORITHM = "sha256";
        public const string HASHCODES_FILES_REGEX = "|^META-INF/hashcodes-\\w+.xml$|";

        private string filename;

        public BdocContainer(string filename)
        {
            this.filename = filename;
        }

        public static string DatafileHashcode(string content)
        {
            var computedhash = StringHelper.ComputeHash("sha256", content);

            var hash256 = content.GetHashSha256();
            var encodedhash256 = hash256.Base64Encode();

            return computedhash;
            //            return content.GetHashSha256().Base64Encode();
        }

        public IFileContainer WriteAsHashcodes(string hashcodesFilename)
        {
            throw new NotImplementedException();            
        }

        public bool IsDataFile(string filenameParam)
        {
            throw new NotImplementedException();                        
        }

        private void WriteHashcodes(object zip, List<IDataFile> datafiles)
        {
            throw new NotImplementedException();                                    
        }

        public List<IDataFile> GetDataFiles()
        {
            throw new NotImplementedException();                                    
        }

        // private function writeComment (\ZipArchive $zip, $comment)
        // private static function containerComment ()

        public IFileContainer WriteWithDataFiles(string bdocFilename, List<IDataFile> dataFiles)
        {
            throw new NotImplementedException();
        }

        private void DeleteHashcodeFiles(object zip)
        {
            throw new NotImplementedException();            
        }

        private bool IsHashcodeesFile(string filename)
        {
            throw new NotImplementedException();            
            
        }

        public bool IsHashcodesFormat()
        {
            throw new NotImplementedException();            
            
        }

        public string GetContainerFormat()
        {
            return "BDOC 2.1";
        }
    }
}