﻿namespace DigiDoc_backend.DigiDoc
{
    using System.Collections.Generic;

    public interface IContainer
    {
        List<IDataFile> GetDataFiles();

        bool IsHashcodesFormat();

        string ToString();
    }
}