﻿namespace DigiDoc_backend.DigiDoc
{
    using System.Collections.Generic;

    public interface IStringContainer
    {
        IStringContainer ToHashcodeFormat();

        IStringContainer ToDataFilesFormat(List<IDataFile> dataFiles);
    }
}